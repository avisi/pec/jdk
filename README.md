# Avisi JDK base images

> Deprecated: please use the java images from [avisi/base/java](https://gitlab.com/avisi/base) instead.

This repository contains the Avisi PEC JDK Docker base image.

[![pipeline status](https://gitlab.com/avisi/pec/jdk/badges/master/pipeline.svg)](https://gitlab.com/avisi/pec/jdk/commits/master)

---

### Tags

| Tag | Notes |
|-----|-------|
| `8-openjdk` | OpenJDK v1.8 |
| `8-openjdk-devel` | OpenJDK v1.8 for development |
| `11-openjdk` | OpenJDK v11.0.1 |

## Usage

```dockerfile
FROM registry.gitlab.com/avisi/pec/jdk:8-openjdk

```

## Issues

All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/pec/jdk/issues).

## License

[MIT © Avisi B.V.](LICENSE)
